package com.quanglan.loginsdt;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class LTT extends AppCompatActivity {

    DatabaseReference mData;

    Button  btnUpdate;
    EditText edtName, edtTN, edtSN;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ltt);



        btnUpdate = findViewById(R.id.buttonUpdate);
        edtName = findViewById(R.id.editTextName);
        edtTN = findViewById(R.id.editTextTN);
        edtSN = findViewById(R.id.editTextSN);

        mData = FirebaseDatabase.getInstance().getReference();
//         mData.child("Name").setValue("Vũ Đình Thắng");


        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mData.child("Name").setValue(edtName.getText().toString());
                mData.child("TN").setValue(edtTN.getText().toString());
                mData.child("SN").setValue(edtSN.getText().toString());

                Intent intent = new Intent(LTT.this, TTKH.class);
                startActivity(intent);
            }
        });
    }
}
