package com.quanglan.loginsdt;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;

import java.util.concurrent.TimeUnit;

public class MainActivity extends AppCompatActivity {

    EditText editTextNumber, editTextOTP;
    Button btnFinish;
    FirebaseAuth mAuth;
    String codeSent;
    TextView txtDK;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mAuth = FirebaseAuth.getInstance();

        editTextOTP = findViewById(R.id.editTextOTP);
        editTextNumber = findViewById(R.id.editTextNumber);

        btnFinish = findViewById(R.id.buttonFinish);
        txtDK = findViewById(R.id.textViewDangKy);

        findViewById(R.id.buttonNext).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendNext();

            }
        });

        findViewById(R.id.buttonFinish).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                verifyFinishCode();

            }
        });

    }

    private void  verifyFinishCode(){

        String code = editTextOTP.getText().toString();

        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(codeSent, code);
        signInWithPhoneAuthCredential(credential);
    }

    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // here you can open activity

                            Intent intent = new Intent(MainActivity.this, LTT.class);
                            startActivity(intent);

                            Toast.makeText(MainActivity.this, "login successful", Toast.LENGTH_SHORT).show();

                        } else {
                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                Toast.makeText(MainActivity.this, "Incorrect Verification Code", Toast.LENGTH_SHORT).show();
                            }

                        }
                    }
                });
    }

    private  void sendNext(){

        String phone = convertNationalPhoneToInternational(editTextNumber.getText().toString());// truyền vào chuỗi String nguyên bản để convert sang số quốc tế

        if(editTextNumber.getText().toString().isEmpty()){  // check ô sđt dã nhập hay chưa
            editTextNumber.setError("Phone number is required");
            editTextNumber.requestFocus();
            return;
        }

        // FIXME: 3/26/2020 format được số điện thoại thì mở phần này ra

        if(editTextNumber.getText().toString().length()!=10){  // check ô số điện thoại có độ dài 10 sô
            editTextNumber.setError("please enter a valid phone");
            editTextNumber.requestFocus();
            return;
        }

        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phone,        // Phone number to verify
                60,                 // Timeout duration
                TimeUnit.SECONDS,   // Unit of timeout
                this,               // Activity (for callback binding)
                mCallbacks);        // OnVerificationStateChangedCallbacks

    }

    PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

        @Override
        public void onVerificationCompleted(@NonNull PhoneAuthCredential phoneAuthCredential) {
            // đây là vị trí khi call API gọi verify code thành công
            Toast.makeText(MainActivity.this, "Thành công", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onVerificationFailed(@NonNull FirebaseException e) {
            // tương tự đây là chỗ nếu call thất bại kết quả sẽ trả về
            Toast.makeText(MainActivity.this, String.format("Fail : %s", e.getMessage()), Toast.LENGTH_LONG).show();
            Log.e("Fail", e.getMessage().toString());
        }

        @Override
        public void onCodeSent(@NonNull String s, @NonNull PhoneAuthProvider.ForceResendingToken forceResendingToken) {
            super.onCodeSent(s, forceResendingToken);

            codeSent = s;
        }
    };

    public static String convertNationalPhoneToInternational(String phone) {
        if (!TextUtils.isEmpty(phone) && phone.length() > 3) {
            if (phone.substring(0, 1).equals("0")) {
                return String.format("+84%s", phone.substring(1));
            }
        }
        return "";
    }

}
